KEYS_PATH=/home/krouma/cert
NVIDIA_DRIVER_VERSION=$(rpm -qa nvidia-driver | awk '{ print $3 }' FS='-' OFS='-')

if [ -z $1 ]; then
    KERNEL_VERSION=$(uname -r)
else
    source /etc/os-release
    KERNEL_VERSION="${1}.fc${VERSION_ID}.$(uname -m)"
fi

MODULES_PATH=/usr/lib/modules/$KERNEL_VERSION/extra

sudo dkms install -m nvidia -k $KERNEL_VERSION -v $NVIDIA_DRIVER_VERSION

for MODULE in nvidia nvidia-drm nvidia-uvm nvidia-modeset
do
	sudo xz --decompress ${MODULES_PATH}/${MODULE}.ko.xz
	sudo /usr/src/kernels/${KERNEL_VERSION}/scripts/sign-file sha256 ${KEYS_PATH}/krouma.priv ${KEYS_PATH}/krouma_pub.der ${MODULES_PATH}/${MODULE}.ko
	sudo xz --compress ${MODULES_PATH}/${MODULE}.ko
done

