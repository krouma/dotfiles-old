-- default desktop configuration for Fedora
-- edited by krouma
module Main {-# LANGUAGE NamedFieldPuns #-} where

import System.Posix.Env (getEnv)
import Data.Maybe (maybe)

import XMonad
import XMonad.Config.Desktop
import XMonad.Config.Gnome
import XMonad.Config.Kde
import XMonad.Config.Xfce

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Layout.NoBorders
import XMonad.Layout.ToggleLayouts
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys,removeKeys)
import qualified XMonad.StackSet as W                       -- Switching between ws
import qualified Data.Map as M
import System.IO
import Graphics.X11.ExtraTypes.XorgDefault(xK_ecaron, xK_scaron, xK_ccaron, xK_rcaron, xK_zcaron)
import Graphics.X11.ExtraTypes.XF86

themeFgDarkerColor    = "#657b83"
themeBgColor          = "#eee8d5"
themeGreenColor       = "#759900"
themeRedColor         = "#dc322f"
themeBlueColor        = "#2aa198"
themeBrightBlueColor  = "#2aa1d8"

main = do
     session <- getEnv "DESKTOP_SESSION"
     let sessionConfig = maybe desktopConfig desktop session
     xmproc <- spawnPipe $ "/usr/bin/xmobar ~/.xmonad/xmobarrc"
     xscproc <- spawnPipe $ "xscreensaver"
     pkproc <- spawnPipe $ "/usr/libexec/polkit-mate-authentication-agent-1"
     picom <- spawnPipe "picom"
     xmonad $ ewmh $ sessionConfig
         { modMask    = myModMask                          -- Rebind Mod to the Windows key
         , terminal   = "mate-terminal"                    -- Change terminal emulator
         , focusedBorderColor = themeFgDarkerColor
         , handleEventHook = handleEventHook desktopConfig <+> fullscreenEventHook
         , layoutHook = myLayoutHook
         , logHook = dynamicLogWithPP xmobarPP              -- Hook for xmobar
             { ppOutput = hPutStrLn xmproc
             , ppTitle = xmobarColor themeGreenColor "" . shorten 64        -- formatting window title
             , ppCurrent = xmobarColor themeBrightBlueColor "" . wrap "[" "]"     -- formatting current ws
             }
         } `removeKeys` myRemoveKeys `additionalKeys` myKeys


myKeys = [ ((0, xF86XK_AudioLowerVolume ), spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%")
         , ((0, xF86XK_AudioRaiseVolume ), spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%")
         , ((shiftMask, xF86XK_AudioLowerVolume ), spawn "pactl set-source-volume @DEFAULT_SOURCE@ -5%")
         , ((shiftMask, xF86XK_AudioRaiseVolume ), spawn "pactl set-source-volume @DEFAULT_SOURCE@ +5%")
         , ((0, xF86XK_AudioMute), spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
         , ((0, xF86XK_AudioPlay), spawn "playerctl play-pause")
         , ((0, xF86XK_AudioPrev), spawn "playerctl previous")
         , ((0, xF86XK_AudioNext), spawn "playerctl next")
         , ((0, xF86XK_MonBrightnessDown), spawn "brightlight -pd 5")
         , ((0, xF86XK_MonBrightnessUp), spawn "brightlight -pi 5")
         , ((0, xF86XK_PowerOff), spawn "systemctl suspend")
         , ((0, xF86XK_Sleep), spawn "systemctl suspend")
         , ((0, xF86XK_WLAN), spawn "toggle_wifi.sh")
         , ((myModMask, xK_p), spawn "j4-dmenu-desktop")
         , ((myModMask, xK_d), spawn "display.sh")
         , ((myModMask .|. shiftMask, xK_l), spawn "xscreensaver-command -activate")
         ] ++
         [((m .|. myModMask, k), windows $ f i)                               -- switching ws
             | (i, k) <- zip [show n | n <- [1..9]]
                 [xK_plus, xK_ecaron, xK_scaron, xK_ccaron, xK_rcaron, xK_zcaron
                 ,xK_yacute, xK_aacute, xK_iacute]
             , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
         ]

myRemoveKeys = [(myModMask, xK_p)]

myLayoutHook = avoidStruts $ toggleLayouts (noBorders Full) $ smartBorders $ myLayouts

myLayouts = Tall 1 (3/100) (1/2)
            -- ||| Mirror (Tall)
            ||| Full


myModMask :: KeyMask
myModMask = mod4Mask


desktop "gnome" = gnomeConfig
desktop "kde" = kde4Config
desktop "xfce" = xfceConfig
desktop "xmonad-mate" = gnomeConfig
desktop _ = desktopConfig

changeKeys :: XConfig a -> (KeyMask, KeySym) -> (KeyMask, KeySym) -> X () -> XConfig a
changeKeys conf orig new commNew = undefined
--    where commOrig = (keys conf) M.! orig
